Se agrego apartado de mascotas perdidas 

## Guia 

Vamos a *mascotas_node-master* y ejecutamos `sudo npm install` para otorgar permisos de administrador (en caso de linux)  
lo mismo hacemos en *mascotas_react-master* 

**Despues para montar las bases de datos vamos a usar docker**   
 por un lado para levantar la base de datos de redis  
> docker run -d --name mascotas-redis -d -p 6379:6379 redis:5.0.9-buster    
 
 por otro lado para levantar la base de datos de mongo  
> docker run -d --name mascotas-mongo -d -p 27017:27017 mongo:4.0.18-xenial 

Una vez realizadas estas acciones tenemos que hacer `npm start` en *mascotas_node-master* (si hay problemas entonces dar permisos de administrador)    

Despues hacer lo mismo en *mascotas_react-master*

y en el puerto 4200 ya estara listo el programa para funcionar  
   
## Guia Mascotas Perdidas

acceder a una mascota y ponerle estado = "Perdido"


