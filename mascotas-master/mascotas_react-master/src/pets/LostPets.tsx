import React, { useState, useEffect } from "react"
import { Pet, loadPetsLost } from "./petsService"
import "../styles.css"
import { useErrorHandler } from "../common/utils/ErrorHandler"
import { goHome } from "../common/utils/Tools"
import FormButtonBar from "../common/components/FormButtonBar"
import FormButton from "../common/components/FormButton"
import FormTitle from "../common/components/FormTitle"
import GlobalContent from "../common/components/GlobalContent"
import { RouteComponentProps } from "react-router-dom"
import Form from "../common/components/Form"

export default function LostPets(props: RouteComponentProps) {
    const [pets, setPets] = useState<Pet[]>([])

    const errorHandler = useErrorHandler()

    const loadLostPets = async () => {
        try {
            const result = await loadPetsLost()
            setPets(result)
        } catch (error) {
            errorHandler.processRestValidations(error)
        }
    }

    useEffect(() => {
        void loadLostPets()
        // eslint-disable-next-line
    }, [])

    return (
        <Form>
            <FormTitle>Mascotas Perdidas</FormTitle>
            <table id="mascotas" className="table">
                <thead>
                    <tr>
                        <th> Nombre </th>
                        <th> Descripción </th>
                        <th> Lugar Mascota Perdida </th>
                        <th> Fecha Mascota Perdida </th>
                        <th> Recompensa </th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    {pets.map((pet, i) => {
                        return (
                            <tr key={i}>
                                <td>{pet.name}</td>
                                <td>{pet.description}</td>
                                <td>{pet.placeLost}</td>
                                <td>{pet.lostDate}</td>
                                <td>{pet.rewardLost}</td>      
                            </tr>
                        )
                    })}
                </tbody>
            </table>

            <FormButtonBar>
                <FormButton label="Cancelar" onClick={() => goHome(props)} />
            </FormButtonBar>
        </Form>
    )
}
