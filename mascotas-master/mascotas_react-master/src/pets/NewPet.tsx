import React, { useEffect, useState } from "react"
import { useErrorHandler } from "../common/utils/ErrorHandler"
import { goHome } from "../common/utils/Tools"
import "../styles.css"
import { deletePet, loadPet, newPet, savePet } from "./petsService"
import DangerLabel from "../common/components/DangerLabel"
import FormInput from "../common/components/FormInput"
import FormInputHidden from "../common/components/FormInputHidden"
import FormButtonBar from "../common/components/FormButtonBar"
import FormAcceptButton from "../common/components/FormAcceptButton"
import FormButton from "../common/components/FormButton"
import FormWarnButton from "../common/components/FormWarnButton"
import FormTitle from "../common/components/FormTitle"
import Form from "../common/components/Form"
import FormSelect from "../common/components/FormSelect"
import GlobalContent from "../common/components/GlobalContent"
import { RouteComponentProps } from "react-router-dom"

export default function NewPet(props: RouteComponentProps<{ id: string }>) {
    const [birthDate, setBirthDate] = useState("")
    const [description, setDescription] = useState("")
    const [petId, setPetId] = useState("")
    const [name, setName] = useState("")
    const [statePet, setStatePet] = useState("")
    const [rewardLost, setRewardLost] = useState("")
    const [lostDate, setLostDate] = useState("")
    const [placeLost, setPlaceLost] = useState("")


    const errorHandler = useErrorHandler()

    const loadPetById = async (id: string) => {
        if (id) {
            try {
                const result = await loadPet(id)
                setBirthDate(result.birthDate)
                setPetId(result.id)
                setName(result.name)
                setDescription(result.description)
                setStatePet(result.statePet)
                setRewardLost(result.rewardLost)
                setLostDate(result.lostDate)
                setPlaceLost(result.placeLost)

            } catch (error) {
                errorHandler.processRestValidations(error)
            }
        }
    }
    const deleteClick = async () => {
        if (petId) {
            try {
                await deletePet(petId)
                props.history.push("/pets")
            } catch (error) {
                errorHandler.processRestValidations(error)
            }
        }
    }

    const saveClick = async () => {
        errorHandler.cleanRestValidations()
        if (!name) {
            errorHandler.addError("name", "No puede estar vacío")
        }

        if (errorHandler.hasErrors()) {
            return
        }

        try {
            if (petId) {
                await savePet({ id: petId, name, birthDate, description, statePet, lostDate, rewardLost,  placeLost })
            } else {
                await newPet({ id: petId, name, birthDate, description, statePet, lostDate, rewardLost,  placeLost })
            }
            props.history.push("/pets")
        } catch (error) {
            errorHandler.processRestValidations(error)
        }
    }

    useEffect(() => {
        const id = props.match.params.id
        if (id) {
            void loadPetById(id)
        }
        // eslint-disable-next-line
    }, []) 
    return (
        
        <GlobalContent>
            <FormTitle>Nueva Mascota</FormTitle>

            <Form>
                <FormInput
                    label="Nombre"
                    name="name"
                    value={name}
                    onChange={event => setName(event.target.value)}
                    errorHandler={errorHandler} />

                <FormInput
                    label="Descripción"
                    name="description"
                    value={description}
                    onChange={event => setDescription(event.target.value)}
                    errorHandler={errorHandler} />

                <FormInput
                    label="Fecha de Nacimiento"
                    name="birthDate"
                    value={birthDate}
                    onChange={event => setBirthDate(event.target.value)}
                    errorHandler={errorHandler} />

                <FormSelect
                    label="Estado de la Mascota"
                    name="statePet"
                    value={statePet}
                    onChange={event => setStatePet(event.target.value)}
                    errorHandler={errorHandler} />
            
                <FormInputHidden
                    label="Fecha en la que la Mascota se Perdio"
                    name="lostDate"
                    hidden={statePet!=="Perdido"}
                    value={lostDate}
                    onChange={event => setLostDate(event.target.value)}
                    errorHandler={errorHandler} />

                <FormInputHidden
                    label="Recompensa por Mascota Perdida"
                    name="rewardLost"
                    hidden={statePet!=="Perdido"}
                    value={rewardLost}
                    onChange={event => setRewardLost(event.target.value)}
                    errorHandler={errorHandler} />

                <FormInputHidden
                    label="Lugar donde se perdio la mascota"
                    name="placeLost"
                    hidden={statePet!=="Perdido"}
                    value={placeLost}
                    onChange={event => setPlaceLost(event.target.value)}
                    errorHandler={errorHandler} />


                <DangerLabel message={errorHandler.errorMessage} />

                <FormButtonBar>
                    <FormAcceptButton label="Guardar" onClick={saveClick} />

                    <FormWarnButton hidden={!petId} label="Eliminar" onClick={deleteClick} />

                    <FormButton label="Cancelar" onClick={() => goHome(props)} />

                </FormButtonBar>
            </Form >
        </GlobalContent>
    )
}
