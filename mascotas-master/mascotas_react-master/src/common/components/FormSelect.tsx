import React from "react"
import { ErrorHandler } from "../utils/ErrorHandler"
import ErrorLabel from "./ErrorLabel"


interface FormSelectProps {
    label: string,
    name: string,
    errorHandler: ErrorHandler,
    value?: string | undefined,
    onChange: (e: React.ChangeEvent<HTMLSelectElement>) => any
}

export default function FormSelect(props: FormSelectProps) {
    return (
        <div className="form-group">
            <label>{props.label}</label>
            <select id={props.name}
                onChange={props.onChange}
                value={props.value}
                className={props.errorHandler.getErrorClass(props.name, "form-control")}>
                
                <option value="No Perdido">
                No Perdido
              </option>
              <option value="Perdido">
                Perdido
              </option>
          </select>
            <ErrorLabel message={props.errorHandler.getErrorText(props.name)} />
        </div>
    )
}