import React from "react"
import { ErrorHandler } from "../utils/ErrorHandler"
import ErrorLabel from "./ErrorLabel"

interface FormInputHiddenProps {
    label: string,
    name: string,
    hidden: boolean,
    errorHandler: ErrorHandler,
    value?: string | undefined | number,
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => any
}

export default function FormInputHidden(props: FormInputHiddenProps) {
    return (
        <div hidden={props.hidden} className="form-group">
            <label>{props.label}</label>
            <input id={props.name}
                value={props.value}
                onChange={props.onChange}
                className={props.errorHandler.getErrorClass(props.name, "form-control")}>
            </input>
            <ErrorLabel message={props.errorHandler.getErrorText(props.name)} />
        </div>
    )
}